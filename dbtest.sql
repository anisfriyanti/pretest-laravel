/*
 Navicat Premium Data Transfer

 Source Server         : dbtest
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : dbtest

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 23/02/2021 07:13:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name_company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of company
-- ----------------------------
BEGIN;
INSERT INTO `company` VALUES (1, 'dsds');
INSERT INTO `company` VALUES (2, 'dsfsf');
COMMIT;

-- ----------------------------
-- Table structure for document
-- ----------------------------
DROP TABLE IF EXISTS `document`;
CREATE TABLE `document` (
  `id` varchar(25) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `type` varchar(15) NOT NULL,
  `folder_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `share` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `owner_id` int DEFAULT NULL,
  `company_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of document
-- ----------------------------
BEGIN;
INSERT INTO `document` VALUES ('82bfdfd2-329ef0de045d', 'f', 'document', 'ab1', 'hu', '', NULL, 1, 1, '2021-02-23 05:35:15', '2021-02-23 06:50:45');
INSERT INTO `document` VALUES ('ewrw', 'rrwrw', 'documnet', 'ab000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `document` VALUES ('te1', 'fsf', 'document', 'ab000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for folder
-- ----------------------------
DROP TABLE IF EXISTS `folder`;
CREATE TABLE `folder` (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `timestamp` int DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `share` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_id` int unsigned NOT NULL,
  `company_id` int unsigned NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`folder_id`) USING BTREE,
  KEY `folder_owner_id_foreign` (`owner_id`),
  KEY `folder_company_id_foreign` (`company_id`),
  CONSTRAINT `folder_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE,
  CONSTRAINT `folder_owner_id_foreign` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of folder
-- ----------------------------
BEGIN;
INSERT INTO `folder` VALUES ('1', 'folder', 'data filsa', 'ab000001', NULL, 1, NULL, NULL, 1, 1, NULL, NULL);
INSERT INTO `folder` VALUES ('23', 'folder', 'data an', 'ab000002', NULL, 1, NULL, NULL, 1, 1, NULL, NULL);
INSERT INTO `folder` VALUES ('457a6f-60cc-4403-8fd2-329ef0de0d3d', 'folder', 'Folder Barud', 'ab000003', NULL, NULL, NULL, NULL, 1, 1, '2021-02-23 04:07:21', '2021-02-22 17:21:52');
INSERT INTO `folder` VALUES ('457a6f29ef0de0d3d', 'folder', 'Folder hgBarud', 'ab000004', NULL, NULL, NULL, NULL, 1, 1, '2021-02-23 04:17:17', '2021-02-23 04:16:30');
INSERT INTO `folder` VALUES ('457a6fvcv29ef0de0d3d', 'folder', 'Folder hgBarud', 'ab000007', NULL, NULL, NULL, NULL, 1, 1, '2021-02-23 06:42:45', '2021-02-23 06:39:09');
INSERT INTO `folder` VALUES ('457cdedsef0de0d3d', 'folder', 'Folder hgBarud', 'ab000007', NULL, NULL, NULL, NULL, 1, 1, '2021-02-23 07:01:31', '2021-02-23 06:46:46');
INSERT INTO `folder` VALUES ('457cdeef0de0d3d', 'folder', 'Folder hgBarud', 'ab000007', NULL, NULL, NULL, NULL, 1, 1, '2021-02-23 06:42:52', '2021-02-23 06:42:52');
INSERT INTO `folder` VALUES ('82b07a6f-60cc-4403-8fd2-329ef0de0d3d', 'folder', 'Folder Baru', 'ab000005', NULL, NULL, NULL, NULL, 1, 1, '2021-02-22 17:17:24', '2021-02-22 17:17:24');
INSERT INTO `folder` VALUES ('adffg', 'folder', 'data B', 'ab000006', NULL, 1, NULL, NULL, 1, 1, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
BEGIN;
INSERT INTO `migrations` VALUES (1, '2021_02_22_042830_create_company', 1);
INSERT INTO `migrations` VALUES (2, '2021_02_22_043129_create_users', 2);
INSERT INTO `migrations` VALUES (3, '2021_02_22_044118_create_folder', 3);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userimage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `iss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aud` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iat` int DEFAULT NULL,
  `exp` int DEFAULT NULL,
  `company_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_key_unique` (`api_key`),
  KEY `users_company_id_foreign` (`company_id`),
  CONSTRAINT `users_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'anis', 'anis@gmail.com', '$2y$10$pVzIwPF6G6/ga8qBZgiPxeB3a.JbOJqY9Cvq0d2TPjZNQwWqFrxPu', NULL, 'ZDk5cW1pdnlCNFNubTNRV1dmTExzNnY1YUJwd0hQN1kyWGsySGRCWA==', NULL, '2021-02-16 12:30:58', '2021-02-23 07:00:48', NULL, NULL, NULL, NULL, NULL, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

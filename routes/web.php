<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*$router->get('/', function () use ($router) {
    return $router->app->version();
});
*/
$router->get('/','OwnerController@getdata');
$router->get('login/','UsersController@authenticate');
$router->get('todo/', 'TodoController@index');

    $router->post('todo/','TodoController@store');
    $router->get('todo/{id}/', 'TodoController@show');
    $router->put('todo/{id}/', 'TodoController@update');
    $router->delete('todo/{id}/', 'TodoController@destroy');
    $router->get('document-service/', 'FolderController@index');
    $router->post('document-service/folder','FolderController@store');
    $router->delete('document-service/folder', 'FolderController@delete');
    $router->get('document-service/folder/{id}/','FolderController@listperfolder');
    $router->get('document-service/document/{id}/', 'FolderController@detaildocument');
    $router->delete('document-service/document','FolderController@deletedoc');
    $router->post('document-service/document','FolderController@storedocument');
    
  
   
    /*
    $app->get('todo/', 'TodoController@index');
    $app->get('todo/{id}/', 'TodoController@show');
    $app->put('todo/{id}/', 'TodoController@update');
    $app->delete('todo/{id}/', 'TodoController@destroy');
    */
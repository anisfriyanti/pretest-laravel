<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model 
{
     //
     //use AuthenticableTrait;
     protected $fillable = ['username','email','password','userimage'];
 
     protected $hidden = [
     'password'
     ];
 
     /*
     * Get Todo of User
     *
     */
     public function todo()
     {
         return $this->hasMany('App\Todo','user_id');
     }
     public function b1()
    {
        return $this->hasMany('App\Folder', 'company_id');
    }
    public function b2()
    {
        return $this->hasMany('App\Folder', 'owner_id');
    }
     public function folder(){
        //return $this->hasOne('App\Folder', 'owner_id', 'company_id');
        //$data = collect([$this->b1, $this->b2]);
        //return $data->unique();
         return $this->hasMany('App\Folder','company_id');
         
         
         
         //return $this->belongsToMany('App\Folder', 'company_id','owner_id');
          }
}

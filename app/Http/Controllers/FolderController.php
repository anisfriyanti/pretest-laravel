<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Folder;
use App\Document;
use Auth;
use Validator;
class FolderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $id = Auth::user()->id;
        $company=Auth::user()->company_id;
    }

    //
    public function index(Request $request)
    {
       // $todo = Auth::user()->b1()->get();
       $id = Auth::user()->id;
       $company=Auth::user()->company_id;

       $stocks = DB::table('folder')
            ->where('owner_id', '=', $id)
            ->where('company_id', '=', $company)
            ->get();
       
       return response()->json(['error' => false,'data' => $stocks]);
    }
    public function store(Request $request){

        $id = Auth::user()->id;
        $company=Auth::user()->company_id;

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required',
            'timestamp' => 'integer'
        ]);
        

             if ($validator->fails()) {
                echo "yes";
            } else {
                $id_input=$request->input('id');
               $name=$request->input('name');
               $timestamp=$request->input('timestamp');
               //$test=(int)request()->get('color_id');
               
               $storefolder = Folder::pluck('folder_id')->last();
               
               list($mem_prefix,$mem_num) = sscanf($storefolder,"%[A-Za-z]%[0-9]");
               $laststore= $mem_prefix . str_pad($mem_num + 1,6,'0',STR_PAD_LEFT);
          
             $checker = Folder::select('id')->where('id',$id_input)->exists();
            
             if($checker!=1){
                $pickup = Folder::create([
                    'id'=> $id_input, 
                    'type'=> 'folder', 
                   'timestamp'=> $timestamp, 
                    'name'=>  $name, 
                    'owner_id'=> $id,
                     'company_id'=> $company,
                     'folder_id'=>$laststore
                ]);
                  //$query_insert = DB::table('folder')->insert($data);
                  $array=$this->fetchdata($id_input);
                 $message= 'folder created';
                  
             }else{
                $pickup=  Folder::where('id', $id_input)->update([
                    
                    'type'=> 'folder', 
                 //   'timestamp'=> $timestamp, 
                    'name'=>  $name, 
                    'owner_id'=> $id,
                     'company_id'=> $company
                ] );
                $array=$this->fetchdata($id_input);
                $message= 'folder updated';
             }
             
             return response()->json(['error' => false,'message' =>$message , 'data'=> $array]);
            }   
             /*
            if(Auth::user()->folder()->Create($request->all())){
                return response()->json(['status' => 'success']);
            }else{
                return response()->json(['status' => 'fail']);
            }
            */
    }
    private function fetchdata($id){
        $todo = Folder::where('id', $id)->get();
        return $todo;
    }
    public function listperfolder($data){
        $id= Auth::user()->id;
       $company=Auth::user()->company_id;
      
       
        $todo = \DB::table('document') 
        ->join('folder', 'folder.folder_id', '=', 'document.folder_id')
            ->select('document.id','document.name','document.type','folder.folder_id','document.content','document.timestamp','document.owner_id','document.share')
            ->where('document.folder_id','=', $data )
            ->where('folder.owner_id', '=', $id)
                ->where('folder.company_id', '=', $company)
                ->get();

        
            
            return response()->json(['error' => false, 'data'=> $todo]);
               

    }
    public function detaildocument($id_doc){
        $id= Auth::user()->id;
        $company=Auth::user()->company_id;
        $todo = \DB::table('document')
        ->join('folder', 'folder.folder_id', '=', 'document.folder_id')
      ->select('document.id','document.name','document.type','folder.folder_id','folder.content','folder.timestamp','folder.owner_id','folder.share')
      ->where('document.id','=', $id_doc )
       ->where('folder.owner_id', '=', $id)
        ->where('folder.company_id', '=', $company)
        ->first();
       
        $data['document']=$todo;
      return response()->json(['error' => false, 'data'=> $data]);

    }
    public function storedocument(Request $request)
    {
       
        $id = Auth::user()->id;
        $company=Auth::user()->company_id;
        
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'type' => 'required',
            
        ]);
        

             if ($validator->fails()) {
                echo "yes";
            } else {
                $id_input=$request->input('id');
               $name=$request->input('name');
               $type=$request->input('type');
               $folder_id=$request->input('folder_id');
               $timestamp=$request->input('timestamp');
               $share=$request->input('share');
               $content=$request->get('content');
               $checker = Document::select('id')->where('id',$id_input)->exists();

               if($checker!=1){
                $pickup = Document::create([
                    'id'=> $id_input, 
                    'type'=> $type, 
                    'name'=>$name,
                    'owner_id'=> $id,
                    'company_id'=> $company,
                    'folder_id'=>$folder_id,
                    'share' => $share,
                      //  'timestamp'=> $timestamp,
                    'content'=>$content
                ]);
                  //$query_insert = DB::table('folder')->insert($data);
                  $array=$this->fetchdatadocument($id_input);
                 $message= 'document created'; 
               }else{
                $pickup=Document::where('id', $id_input)->update([
                    
                    'type'=> $type, 
                    'name'=>$name,
                    'owner_id'=> $id,
                    'company_id'=> $company,
                    'folder_id'=>$folder_id,
                    'share' => $share,
                      //  'timestamp'=> $timestamp,
                    'content'=>$content
                ] );
                $array=$this->fetchdatadocument($id_input);
                $message= 'document updated';
               }
               return response()->json(['error' => false,'message' =>$message , 'data'=> $array]);
            }
    }
    public function delete(Request $request){
       $id = $request->id;
      
        if(Folder::destroy($id)){

            return response()->json([ 'error'=> false,'message' => "Success delete folder"]);
       }
    }
    private function fetchdatadocument($id){
        $todo = Document::where('id', $id)->get();
        return $todo;
    }
    public function deletedoc(Request $request){
        $id = $request->id;
        if(Document::destroy($id)){

            return response()->json([ 'error'=> false,'message' => "Success delete document"]);
       }
    }
    
}
